"use strict";

// TODO Car
var car = {
    Year: "2007",
    Make: "BMW",
    Model: "323i",
    Bodytype: "Sedan",
    Transmission: "Tiptronic",
    Odometer: "68,512",
    Price: "$16,000",

}
for(var property in car){
    var value = car[property];
    console.log(property + ": " + value);
}

// TODO Music
var album = {
    Title: "1989",
    Artist: "Taylor Swift",
    Year: "2014",
    Genre: "Pop",
    getMusicAlbum: function(){
        var musicAlbum = this.Title + ", released in " + this.Year + " by "
        + this.Artist;
        return musicAlbum; 
    },
    trackNames: [
        
    ]
}
